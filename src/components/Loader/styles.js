import styled, { keyframes } from 'styled-components';

export const Container = styled.main`
  width: 100vw;
  height: 100vh;
  background: rgba(0,0,0,.1);
  position: fixed;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const animation = keyframes`
  0% {
    transform: rotate(20deg);
  }
  50% {
    transform: rotate(180deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

export const Circle = styled.div`
  width: 50;
  height: 50;
  animation-name: ${animation};
  animation-fill-mode: backwards;
  animation-timing-function: linear;
  animation-iteration-count: infinite;
  animation-duration: 0.5s;
`;