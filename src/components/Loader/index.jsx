import React from 'react';
import { CircularProgressbar } from 'react-circular-progressbar';
import { Container, Circle } from './styles';
import 'react-circular-progressbar/dist/styles.css';


function Loader() {
  return (
    <Container>
      <Circle>
        <CircularProgressbar value={50} styles={{ root: { width: 50, height: 50 } }}/>
      </Circle>
    </Container>
  )
}

export default Loader;
