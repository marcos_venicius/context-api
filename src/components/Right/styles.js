import styled from 'styled-components';

export const Container = styled.div`
  width: 50%;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;

  @media screen and (max-width: 770px) {
    width: 90%;
    height: 50vh;
  }

  @media screen and (max-height: 320px) {
		height: 350px;
	}
`;

export const Box = styled.div`
  width: 90%;
  height: 80%;
  background: white;
  border: 1px solid lightgray;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;

  /* @media screen and (max-width: 770px) {
    max-height: 50vh;
  } */
`;

export const Header = styled.header`
  width: 100%;
  display: flex;
  align-items: center;
  background: dodgerblue;
  padding: 0.5rem;
`;

export const Title = styled.h1`
	font-size: 2rem;
	color: white;
	flex: 1;
  text-align: center;
`;

export const Preview = styled.div`
  width: 100%;
  flex: 1;
  padding: 1rem 0.5rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  overflow-y: auto;
`;

export const TitlePreview = styled.h3`
  word-break: break-all;
  text-align: center;
`;

export const TextPreview = styled.p`
  word-break: break-all;
  text-align: center;
  margin-top: 1rem;
  max-width: 90%;
`;

export const Placeholder = styled.span`
  color: gray;
  cursor: default;
  pointer-events: none;
  user-select: none;
  -moz-user-select: none;
`;