import React, { useEffect, useState } from 'react';
import Switch from '@material-ui/core/Switch';

import { useText } from '../../contexts/Text';

import {
	Container,
	Box,
	Header,
	Title,
	Preview,
	TitlePreview,
	TextPreview,
	Placeholder
} from './styles';

function Right() {

	const { title, text, preview, setPreview } = useText();
	const [checked, setChecked] = useState(false);
	
	useEffect(() => {
		if (preview.title.length || preview.text.length) {
			setPreview({title: '', text: ''});
		}
	}, [title, text, checked]);

	return (
		<Container>
			<Box>
				<Header>
					<Switch
						checked={checked}
						onChange={() => setChecked(!checked)}
						color="primary"
						inputProps={{ 'aria-label': 'primary checkbox' }}
						title="Enable real time preview"
					/>
					<Title>Preview</Title>
				</Header>
				<Preview>
					<TitlePreview>
						{
							preview.title.length ? preview.title : (
								title && checked ? title : <Placeholder>Type something to title</Placeholder>
							)
						}
					</TitlePreview>
					<TextPreview>
						{
							preview.text.length ? preview.text : (
								text && checked ? text : <Placeholder>Type something to text</Placeholder>
							)
						}
					</TextPreview>
				</Preview>
			</Box>
		</Container>		
	);
}

export default Right;
