import React, { useState } from 'react';
import { nanoid } from 'nanoid';

import { useText } from '../../contexts/Text';

import {
  Container,
  Menu,
  Item,
  ItemTitle,
  ItemText,
  Placeholder,
  Text
} from './styles';

function SideBar() {
  const { allTextSaved, setPreview } = useText();

  const [open, setOpen] = useState(false);

  function handlePreview(item) {
    setPreview({ title: item.title, text: item.text });
    setTimeout(() => {
      setOpen(false);
    }, 200)
  }

  return (
    <>
      <Menu onClick={() => setOpen(!open)}>
        <span />
        <span />
        <span />
      </Menu>
      <Container open={open}>
        {
          allTextSaved.length > 0 ? (
            allTextSaved.map(item => (
              <Item key={nanoid()} onClick={() => handlePreview(item)}>
                <ItemTitle>{item.title}</ItemTitle>
                <ItemText>{item.text}</ItemText>
              </Item>
            ))
          ) : (
            <Placeholder>
              <Text>You not have any cards</Text>
            </Placeholder>
          )
        }
      </Container>
    </>
  )
}

export default SideBar;