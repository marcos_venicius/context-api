import styled from 'styled-components';

export const Container = styled.div`
  height: 100vh;
  width: min(500px, 90vw);

  position: fixed;
  top: 0;
  left: ${props => props.open ? 0 : '-100%'};
  background: #f5f5f5;
  padding: 0.5rem;

  z-index: 999;

  overflow-y: scroll;
  overflow-x: hidden;

  &::-webkit-scrollbar {
    width: 8px;
    height: 8px;
    background: lightgray;
  }

  &::-webkit-scrollbar-thumb {
    background: gray;
  }
`;

export const Menu = styled.div`
  position: fixed;
  top: 10px;
  right: 15px;
  background: transparent;
  z-index: 1000;
  cursor: pointer;

  &:hover span {
    background: #1a1a1d;
  }

  &:active span {
    opacity: 0.5;
  }

  &> span {
    display: block;
    width: 30px;
    height: 5px;
    border-radius: 3px;
    background: #404452;
    pointer-events: none;

    &:not(:first-child) {
      margin-top: 3px;
    }
  }
`;

export const Item = styled.div`
  width: 90%;
  margin: 0 auto;
  margin-top: 0.5rem;
  border: 1px solid lightgray;
  padding: 0.8rem;
  cursor: pointer;
  
  &:hover {
    background: rgba(200,200,200,.2);
  }
`;

export const ItemTitle = styled.h4`
  width: 100%;
  text-align: center;
  color: #404040;
`;

export const ItemText = styled.p`
  width: 100%;
  text-align: center;
  color: #505050;
  margin-top: 1rem;
`;

export const Placeholder = styled.span`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Text = styled.p`
  text-align: center;
  color: rgb(100,100,100);
`;