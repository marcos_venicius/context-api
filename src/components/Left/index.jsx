import React, { useState, memo } from 'react';

import { useText } from '../../contexts/Text';

import {
	Container,
	Box,
	Header,
	Title,
	ContainerInput,
	Input,
	Button,
	Link
} from './styles';

const Left = () => {
	const { setTitle, setText, title, text, saveText } = useText();

	const MAX_TITLE_LENGTH = 100;
	const MAX_TEXT_LENGTH = 200;

	const [titleLength, setTitleLength] = useState(0);
	const [textLength, setTextLength] = useState(0);
	const [titleFocus, setTitleFocus] = useState(false);
	const [textFocus, setTextFocus] = useState(false);

	function handleChangeTitle(e) {
		const value = String(e.target.value);
		const length = String(e.target.value).length;

		if (length <= MAX_TITLE_LENGTH) {
			setTitleLength(length);
			setTitle(value);
		}
	}

	function handleChangeText(e) {
		const value = String(e.target.value);
		const length = String(e.target.value).length;

		if (length <= MAX_TEXT_LENGTH) {
			setTextLength(length);
			setText(value);
		}
	}

	function handleSaveText() {
		setTitleLength(0);
		setTextLength(0);
		saveText();
	}

	return (
		<Container>
			<Box>
				<Header>
					<Title>
						Write anything here
					</Title>
				</Header>
				<div>
					<ContainerInput focus={titleFocus}>
						<span>{ titleLength }</span>
						<Input
							onFocus={() => setTitleFocus(true)}
							onBlur={() => setTitleFocus(false)}
							type="text"
							placeholder="Title"
							value={title}
							onChange={handleChangeTitle}
						/>
					</ContainerInput>
					<ContainerInput focus={textFocus}>
						<span>{ textLength }</span>
						<Input
							onFocus={() => setTextFocus(true)}
							onBlur={() => setTextFocus(false)}
							type="text"
							placeholder="Text"
							value={text}
							onChange={handleChangeText}
						/>
					</ContainerInput>
					<Button onClick={handleSaveText}>
						Done
					</Button>
				</div>
				<Link target="_blank" href="https://www.instagram.com/marcos.dev.web">@marcos.dev.web</Link>
			</Box>
		</Container>
	);
}

export default memo(Left);
