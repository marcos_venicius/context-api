import styled from 'styled-components';

export const Container = styled.main`
	width: 50%;
	height: 100vh;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;

	@media screen and (max-width: 770px) {
    width: 90%;
		height: 50vh;
  }

	@media screen and (max-height: 320px) {
		height: 350px;
	}
`;

export const Box = styled.div`
	width: 90%;
	height: 80%;
	background: white;
	border: 1px solid lightgray;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: space-between;

	&> div {
		width: 100%;
		display: flex;
		flex: 1;
		flex-direction: column;
		align-items: center;
		justify-content: center;
		margin: 0 auto;
	}
`;

export const Header = styled.header`
	width: 100%;
	padding: 0.5rem;
	display: flex;
	align-items: center;
	background: dodgerblue;
`;

export const Title = styled.h1`
	font-size: 2rem;
	color: white;
	width: 100%;
	text-align: center;
`;

export const ContainerInput = styled.div`
	width: 90%;
	margin-top: 0.2rem;

	&> span {
		display: inline-block;
		width: 100%;
		padding-right: 2px;
		color: dodgerblue;
		text-align: right;
		margin-left: auto;
		opacity: ${props => props.focus ? 1 : 0.2};
	}
`;

export const Input = styled.input`
	width: 100%;
	height: 2rem;
	border: 1px solid lightgray;
	outline: none;
	text-align: center;
	padding: 5px;
	font-size: 1.2rem;

	&:focus {
		border-color: dodgerblue;
	}
`;

export const Button = styled.button`
	width: 90%;
	border: 1px solid white;
	padding: 5px;
	font-size: 1rem;
	background: dodgerblue;
	color: white;
	cursor: pointer;
	margin-top: 1rem;

	&:hover {
		opacity: 0.8;
	}

	&:active {
		opacity: 0.5;
	}
`;

export const Link = styled.a`
	color: black;
	text-decoration: none;
	color: gray;
	margin-bottom: 0.5rem;

	&:hover {
		opacity: 0.8;
	}
`;