import styled from 'styled-components';

export const Container = styled.main`
  width: 100vw;
  height: 100vh;
`;

export const Header = styled.header`
  width: 100%;
  height: 5rem;
  display: flex;
  align-items: center;
  justify-content: center;
  border-bottom: 1px solid lightgray;
`;

export const Title = styled.h1`
  color: #304050;
`;

export const Content = styled.div`
  width: 100%;
  height: calc(100vh - 5rem);
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Box = styled.div`
  margin: 2rem auto 0 auto;
  width: min(400px, 95vw);
  padding: 2rem 1rem;
  border: 1px solid lightgray;
  background: white;
`;

export const Input = styled.input`
  width: 100%;
  font-size: 1rem;
  outline: none;
  border: 1px solid lightgray;
  padding: 5px 10px;
  color: #333;

  &:not(:first-child) {
    margin-top: 1rem;
  }

  &:focus {
    border-color: dodgerblue;
  }
`;

export const Button = styled.button`
  margin-top: 1rem;
  width: 100%;
  padding: 5px 10px;
  border: 0;
  background: dodgerblue;
  color: white;
  text-align: center;
  cursor: pointer;
  font-size: 1.1rem;

  &:hover {
    opacity: 0.8;
  }

  &:active {
    opacity: 0.5;
  }
`;