import React, { useState } from 'react';

import Loader from '../../components/Loader';

import {
  Container,
  Header,
  Title,
  Content,
  Box,
  Input,
  Button
} from './styles';

function Login() {
  const [loading, setLoading] = useState(false);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  return (
    <Container>
      {
        loading && (
          <Loader />
        )
      }
      <Header>
        <Title>Sign In</Title>
      </Header>
      <Content>
        <Box>
          <Input
            value={username}
            onChange={e => setUsername(String(e.target.value))}
            type="text"
            placeholder="Username"
            spellCheck="false"
            autoComplete="off"
          />
          <Input
            value={password}
            onChange={e => setPassword(String(e.target.value))}
            type="password"
            placeholder="Password"
            spellCheck="false"
            autoComplete="off"
          />
          <Button>
            Sign In
          </Button>
        </Box>
      </Content>
    </Container>
  );
}

export default Login;
