import styled from 'styled-components';

export const Container = styled.main`
	width: 100vw;
	height: 100vh;
	min-height: 320px;
	overflow-y: scroll;
	overflow-x: hidden;
	display: flex;

	&::-webkit-scrollbar {
		width: 8px;
		height: 8px;
		background: lightgray;
	}

	&::-webkit-scrollbar-thumb {
		background: gray;
	}

	@media screen and (max-width: 770px) {
		flex-direction: column;
		align-items: center;
		justify-content: center;
		min-height: 577px;
	}
`;
