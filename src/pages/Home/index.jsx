import React from 'react';

import Left from '../../components/Left';
import Right from '../../components/Right';
import SideBar from '../../components/SideBar';

import {
	Container
} from './styles';

function Home() {
	return (
    <Container>
      <SideBar />
      <Left />
      <Right />
    </Container>
	);
}

export default Home;