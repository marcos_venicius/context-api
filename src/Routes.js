import React, { useEffect, useState } from 'react';
import { Route, Redirect } from 'react-router-dom';

import { useAuth } from './contexts/Auth';

import Home from './pages/Home';
import Login from './pages/Login';
import Loader from './components/Loader';

function Authenticated() {
  return (
    <Route exact path="/" component={Home} />
  );
}

function NotAuthenticated() {
  return (
    <>
      <Route exact path="/login" component={Login} />
      <Route path="*">
        <Redirect to="/login" />
      </Route>
    </>
  );
}

function Routes() {
  const [loading, setLoading] = useState(true);
  const { signned, authenticate } = useAuth();

  useEffect(() => {
    authenticate();
    setLoading(false);
  }, [])

  if (loading) {
    return (
      <Loader />
    );
  }

  return (
    signned ? (
      <Authenticated />
    ) : (
      <NotAuthenticated />
    )
  );
}

export default Routes;
