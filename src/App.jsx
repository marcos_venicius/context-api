import React from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';

import TextProvider from './contexts/Text';
import AuthProvider from './contexts/Auth';

import Routes from './Routes';

function App() {
	return (
		<BrowserRouter>
			<Switch>
				<AuthProvider>
					<TextProvider>
						<Routes />
					</TextProvider>
				</AuthProvider>
			</Switch>
		</BrowserRouter>
	);
}

export default App;
