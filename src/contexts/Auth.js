import React, { createContext, useContext, useState } from 'react';

const INITIAL_STATE = {
  signned: false,
  authenticate: () => {},
  login: () => {},
  logout: () => {},
}

const AuthContext = createContext(INITIAL_STATE);

function AuthProvider({ children }) {
  const [signned, setSignned] = useState(false);
  const users = [
    {
      username: 'marcos',
      password: 'marcos123'
    },
    {
      username: 'jose',
      password: 'jose123',
    }
  ];

  function authenticate() {
    const settings = window.localStorage.getItem('@app_auth');
    if (settings) {
      try {
        const jsonSettings = JSON.parse(settings)
        for (let user of users) {
          if (jsonSettings.username === user.username && jsonSettings.password === user.password) {
            setSignned(true);
          }
        }
      } catch(_) {
        return;
      }
    }
  }

  function logout() {
    window.localStorage.removeItem('@app_auth');
    setSignned(false);
  }

  function login({ username, password }) {

    for (let user of users) {
      if (username === user.username && password === user.password) {
        const jsonSettings = JSON.stringify({
          username,
          password,
        });
    
        window.localStorage.setItem('@app_auth', jsonSettings);
        setSignned(true);
        return true;
      }
    }

    setSignned(false);
    return false;
  }

  return (
    <AuthContext.Provider value={{ signned, login, authenticate, logout }}>
      { children }
    </AuthContext.Provider>
  )
}

export function useAuth() {
  const context = useContext(AuthContext);
  return context;
}

export default AuthProvider;