import React, { createContext, useState, useContext } from 'react';

const INITIAL_STATE = {
  title: "",
  text: "",
  setTitle: () => {},
  setText: () => {},
  saveText: () => {},
  preview: {}
};

const TextContext = createContext(INITIAL_STATE);

function TextProvider({ children }) {
  const [title, setTitle] = useState('');
  const [text, setText] = useState('');
  const [preview, setPreview] = useState({title: '', text: ''});
  const [allTextSaved, setAllTextSaved] = useState([]);

  function saveText() {
    if (title.length > 0 || text.length > 0) {
      setAllTextSaved(state => ([...state, {
        title,
        text
      }]));

      setTitle('');
      setText('');
    }
  }

  return (
    <TextContext.Provider value={{
      title,
      text,
      setTitle,
      setText,
      saveText,
      allTextSaved,
      preview,
      setPreview
    }}>
      { children }
    </TextContext.Provider>
  )
}

export function useText() {
  const context = useContext(TextContext);
  return context;
}

export default TextProvider;